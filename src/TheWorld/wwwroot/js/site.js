﻿(function () {

    var $sidebarAndWrapper = $('#sidebar,#wrapper');

    $('#sidebarToggle').on('click', function () {
        $sidebarAndWrapper.toggleClass('hide-sidebar');
        
        $(this).text(($sidebarAndWrapper.hasClass("hide-sidebar") ? "Show" : "Hide") + " Sidebar");
        
    }); 

})();